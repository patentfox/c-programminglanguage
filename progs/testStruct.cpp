#include <iostream>
using namespace std;

struct A {
   int a;
   float b;
};

class B {
public:
   int a;
   float b;
};

int main() {
   A obja = {1, 2.2};
   cout << obja.a << endl; 

   B objb = {1, 2.2};
   cout << objb.a << endl; 
}