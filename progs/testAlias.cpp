#include <iostream>
#include "Vector_t.h"
#include <map>
using namespace std;

template <typename C>
using Element_type = typename C::value_type;

template <typename Container>
void algo(const Container &c) {
   Vector<Element_type<Container>> v(10);
   // more stuff
}

template <typename Value>
using String_map = map<string, Value>;

int main() {
   Vector<int> v{1,2,3};
   algo(v);

   String_map<int> stoi;
}