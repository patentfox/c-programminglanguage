#include "StrBlob.h"
using namespace std;

StrBlob::StrBlob(): data(make_shared<vector<string>>()) { }

// il can be passed directly as make_shared creates a dynamic object in place using constructor of vector.
StrBlob::StrBlob(initializer_list<string> il): data(make_shared<vector<string>>(il)) { }

void StrBlob::check(size_type i, const string& msg) const {
    if(i >= data->size()) {
        throw out_of_range(msg);
    }
}

string& StrBlob::front() {
    check(0, "front on empty StrBlob");
    return data->front();
}

string& StrBlob::back() {
    check(0, "back on empty StrBlob");
    return data->back();
}