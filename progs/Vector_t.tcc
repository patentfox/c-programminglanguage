#include <algorithm>    // for std::copy
#include <stdexcept>
#include <iostream>

template<typename T>
Vector<T>::Vector(int s) {
   std::cout << "Constructor with int argument called" << std::endl;
   if (s < 0)
      throw std::length_error{ "Invoked with negative length" };
   sz = s;
   elem = new T[sz];
}

template<typename T>
Vector<T>::Vector(std::initializer_list<T> lst) : sz{ static_cast<int>(lst.size()) },
elem{ new T[lst.size()] }
{
   std::cout << "Constructor with initializer list called" << std::endl;
   int i = 0;
   for (auto it = lst.begin(); it != lst.end(); ++it) {
      elem[i++] = *it;
   }
}

template<typename T>
Vector<T>::Vector(const Vector<T> &v) : sz{ v.sz }, elem{ new T[v.sz] } {
   std::cout << "Copy constructor called" << std::endl;
   for (int i = 0; i < sz; ++i) {
      elem[i] = v.elem[i];
   }
}

template<typename T>
Vector<T>& Vector<T>::operator=(const Vector<T> &v) {
   std::cout << "Copy assignment called" << std::endl;
   if (elem)
      delete[] elem;
   sz = v.sz;
   elem = new T[sz];
   for (int i = 0; i < sz; ++i) {
      elem[i] = v.elem[i];
   }
   return *this;
}

template<typename T>
Vector<T>::Vector(Vector<T> &&v): sz{v.sz}, elem{v.elem} {
   std::cout << "Move constructor called" << std::endl;
   v.elem = nullptr;
   v.sz = 0;
}

template<typename T>
Vector<T>::~Vector() {
   std::cout << "Destructor called" << std::endl;
   delete[] elem;
}

template<typename T>
T& Vector<T>::operator[](int i) {
   if (i < 0 || i >= size())
      throw std::out_of_range{ "Vector operand[] invalid" };
   return elem[i];
}

template<typename T>
const T& Vector<T>::operator[](int i) const {
   if (i < 0 || i >= size())
      throw std::out_of_range{ "Vector operand[] invalid" };
   return elem[i];
}

template<typename T>
int Vector<T>::size() const {
   return sz;
}

template<typename T>
Vector<T> operator+(const Vector<T> &a, const Vector<T> &b) {
   if (a.size() != b.size()) {
      throw std::logic_error("Vector size mismatch");
   }
   Vector<T> result(a.size());
   for (int i = 0; i < a.size(); ++i) {
      result[i] = a[i] + b[i];
   }
   return result;
}