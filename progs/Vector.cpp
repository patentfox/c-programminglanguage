#include "Vector.h"
#include <algorithm>    // for std::copy
#include <stdexcept>
#include <iostream>
using namespace std;

Vector::Vector(int s) {
   cout << "Constructor with int argument called" << endl;
   if (s < 0)
      throw std::length_error{ "Invoked with negative length" };
   sz = s;
   elem = new double[sz];
}

Vector::Vector(std::initializer_list<double> lst) : sz{ static_cast<int>(lst.size()) },
elem{ new double[lst.size()] }
{
   cout << "Constructor with initializer list called" << endl;
   int i = 0;
   for (auto it = lst.begin(); it != lst.end(); ++it) {
      elem[i++] = *it;
   }
}

Vector::Vector(const Vector &v) : sz{ v.sz }, elem{ new double[v.sz] } {
   cout << "Copy constructor called" << endl;
   for (int i = 0; i < sz; ++i) {
      elem[i] = v.elem[i];
   }
}

Vector& Vector::operator=(const Vector &v) {
   cout << "Copy assignment called" << endl;
   if (elem)
      delete[] elem;
   sz = v.sz;
   elem = new double[sz];
   for (int i = 0; i < sz; ++i) {
      elem[i] = v.elem[i];
   }
   return *this;
}

Vector::Vector(Vector &&v): sz{v.sz}, elem{v.elem} {
   cout << "Move constructor called" << endl;
   v.elem = nullptr;
   v.sz = 0;
}

Vector::~Vector() {
   cout << "Destructor called" << endl;
   delete[] elem;
}

double& Vector::operator[](int i) {
   if (i < 0 || i >= size())
      throw std::out_of_range{ "Vector operand[] invalid" };
   return elem[i];
}

const double& Vector::operator[](int i) const {
   if (i < 0 || i >= size())
      throw std::out_of_range{ "Vector operand[] invalid" };
   return elem[i];
}

int Vector::size() const {
   return sz;
}

Vector operator+(const Vector &a, const Vector &b) {
   if (a.size() != b.size()) {
      throw std::logic_error("Vector size mismatch");
   }
   Vector result(a.size());
   for (int i = 0; i < a.size(); ++i) {
      result[i] = a[i] + b[i];
   }
   return result;
}