#include "Entry.h"
#include <vector>
#include <list>
#include <algorithm>
#include <string>
using namespace std;

void f(vector<Entry> &vec, list<Entry> &lst) {
   sort(vec.begin(), vec.end());
   unique_copy(vec.begin(), vec.end(), back_inserter(lst));
}

bool has_c(string s, char c) {
   return find(s.begin(), s.end(), c) != s.end();
}

// vector<string::iterator> find_all(string &s, char c) {
//    vector<string::iterator> vec;
//    for(auto it = s.begin(); it!= s.end(); ++it) {
//       if(*it == c) {
//          vec.push_back(it);
//       }
//    }
//    return vec;    // this is efficient because of move semantics
// }

template <typename C, typename V>
vector<typename C::iterator> find_all(C &c, V v) {
   vector<typename C::iterator> vec;
   for(auto it = c.begin(); it != c.end(); ++it) {
      if(*it == v) {
         vec.push_back(it);
      }
   }
   return vec;
}

void test() {
   string m {"Mary had a little lamb"};
   cout << "Number of occurrences of 'a' = " << find_all(m, 'a').size() << endl;
}

int main() {
   // vector<Entry> vec = {
   //    {"Kaustubh", 27},
   //    {"Divya", 27},
   //    {"Kanishka", 20}
   // };
   // list<Entry> lst;
   // f(vec, lst);
   // for(auto e: lst) {
   //    cout << e << endl;
   // }

   test();
}