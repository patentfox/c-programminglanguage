#include <iostream>
#include <string>

struct Entry {
   std::string name;
   int number;
};

std::ostream& operator<<(std::ostream &os, const Entry &en);

//read in format {"name", number}
std::istream& operator>>(std::istream &is, Entry &en);

inline bool operator<(const Entry &x, const Entry &y) {
   return x.name < y.name;
}
inline bool operator==(const Entry &x, const Entry &y) {
   return x.name == y.name && x.number == y.number;
}
