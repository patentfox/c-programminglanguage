#include "Container.h"
#include "Vector_container.h"
#include <iostream>
using namespace std;

void use(Container &c) {
	std::cout << c[2] << std::endl;
   cout << sizeof(c) << endl;
}

int main() {
	Vector_container v({ 1, 2, 3, 4, 5 });
	use(v);
}