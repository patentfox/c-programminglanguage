#include "complex.h"
#include <iostream>
using namespace std;

int main() {
  complex z{2,3};
  z = -z;
  cout << z.real() << ", " << z.imag() << endl;
  complex z2{ 1 / -z };
  cout << z2.real() << ", " << z2.imag() << endl;
  return 0;
}