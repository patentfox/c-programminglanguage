#include <iostream>
using namespace std;

class A {
public:
  virtual void f() {
    cout << "Inside A" << endl;
  }
};

class B: public A {
public:
  void f() {
    cout << "Inside B" << endl;
  }
};

class C: public B {
public:
  void f() {
    cout << "Inside C" << endl;
  }
};

void callf(A &ref) {
  ref.f();
}

int main() {
  C obj;
  callf(obj);
}

