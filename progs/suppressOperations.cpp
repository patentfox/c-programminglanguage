#include <iostream>
using namespace std;

class A {
public:
  A() {cout << "A\'s constructor" << endl;}
  A(const A&) {
     cout << "A's copy constructor called" << endl;
  }
  //A(const A&) = delete;

  virtual void f() {
    cout << "Inside A" << endl;
  }
};

class B: public A {
public:
   int m;
   B(){cout << "B\'s constructor" << endl;}
   // B(const B&) {
   //   cout << "B's copy constructor called" << endl;
   // }
   void f() {
      cout << "Inside B" << endl;
   }
};

int main() {
   B b1;
   b1.m = 2;
   B b2(b1);
   cout << b2.m << endl;
}