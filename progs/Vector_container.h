#include "Container.h"
#include "Vector.h"

class Vector_container : public Container {
public:
	Vector_container(int s) : v(s) {}
	Vector_container(std::initializer_list<double> lst) : v{ lst } {}
	~Vector_container() {}  // as v is destroyed by its own destructor
	double& operator[](int i) {
	  return v[i];
	}
	const double& operator[](int i) const {
		return v[i];
	}

   	int size() const { return v.size(); }
private:
  	Vector v;
};
