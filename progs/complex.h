#ifndef _complex_h_
#define _complex_h_

class complex {
public:
   complex(double r, double i) : re{ r }, im{ i } {}
   complex(double r) : complex{ r, 0 } {}
   complex() : complex{ 0, 0 } {}

   double real() const { return re; }
   void real(double d) { re = d; }
   double imag() const { return im; }
   void imag(double d) { im = d; }
   complex& operator+=(complex z) {
      re += z.re;
      im += z.im;
      return *this;
   }
   complex& operator-=(complex z) {
      this->re -= z.re;
      this->im -= z.im;
      return *this;
   }
   complex& operator*=(complex z);
   complex& operator/=(complex z);
private:
   double re, im;
};

inline complex operator+(complex a, complex b) {
   return a += b;
}
inline complex operator-(complex a, complex b) {
   return a -= b;
}
inline complex operator*(complex a, complex b) {
   return a *= b;
}
inline complex operator/(complex a, complex b) {
   return a /= b;
}
inline complex operator-(complex z) {
   return 0 - z;
}

inline bool operator==(complex a, complex b) {
   return a.real() == b.real() && a.imag() == b.imag();
}

inline bool operator!=(complex a, complex b) {
   return !(a == b);
}

#endif