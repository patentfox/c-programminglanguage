#include <iostream>
#include <string>
using namespace std;

istream& foo(istream& is) {
	string s;
	ios_base::iostate before = is.rdstate();
	while (is >> s) {
		cout << s << endl;
	}
	is.clear(before);
	return is;
}

int main() {
	foo(cin);
	return 0;
}