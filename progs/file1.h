extern const int ci;

class X {
public:
   int x = 10;
};

inline X make_x() {
   return X();
}
