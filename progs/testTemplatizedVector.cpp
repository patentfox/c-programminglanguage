#include "Vector_t.h"
#include <iostream>
#include <string>
using namespace std;

template <typename C, typename V>
V sum(const C &container, V value) {
   for(auto x: container) {
      value = value + x;
   }
   return value;
}

void testSum(const Vector<double> &vd, const Vector<string> &vs) {
   double d = sum(vd, 0.0);
   string str = sum<Vector<string>, string>(vs, "");
   cout << d << " " << str << endl;
}

int main() {
   // Vector<int> v{1,2,3};
   // for(auto i: v) {
   //    cout << i << endl;
   // }
   testSum({1.0,2,3}, {"kaustubh ", "bansal"});
}
