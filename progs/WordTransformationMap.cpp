#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <cassert>
#include <sstream>
using namespace std;

map<string,string> buildTransformMap(istream &mapFile) {
	map<string, string> transformMap;
	string line;
	while (getline(mapFile, line)) {
		size_t space = line.find(' ');
		assert(space != string::npos);
		string slang = line.substr(0, space);
		string expansion = line.substr(space + 1);
		transformMap[slang] = expansion;
	}
	return transformMap;
}

void formalify(istream &input, ostream &output,  const map<string, string>& transformMap) {
	string line, outword;
	while (getline(input,line)) {
		istringstream iss(line);
		string word;
		while (iss >> word) {
			if (transformMap.find(word) != transformMap.end())
				output << transformMap.at(word) << " ";
			else
				output << word << " ";
		}
		output << endl;
	}
}

int main() {
	ifstream mapFile("transformation_map.txt");
	ifstream input("input.txt");

	map<string,string> transformMap = buildTransformMap(mapFile);
	formalify(input, cout, transformMap);
	return 0;
}