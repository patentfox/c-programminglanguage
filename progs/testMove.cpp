#include <iostream>
#include "Vector.h"
#include <utility>
using namespace std;

int main() {
   Vector v1{ 1, 2, 3 };
   Vector v2{ 4, 5, 6 };
   Vector v3 = std::move(v1 + v2);

   for (int i = 0; i < v3.size(); ++i) {
      cout << v3[i] << endl;
   }
   //Vector v4 = std::move(v3);
}