#include "Entry.h"
#include <iostream>
#include <ios>
using namespace std;

ostream& operator<<(ostream &os, const Entry &en) {
   os << "{\"" << en.name << string("\", ") << en.number << "}";
   return os;
}

//read in format {"name", number}
istream& operator>>(istream &is, Entry &en) {
   char c1,c2;
   string name;
   int number;
   if(is >> c1 && c1=='{' && is >> c2 && c2=='"') {
      char n;
      while(is.get(n) && n!='"')
         name += n;
      if(is >> c2 && c2 == ',' && is >> number && is >> c1 && c1 == '}') {
         en.number = number;
         en.name = name;
         return is;
      }
   }
   is.setstate(ios_base::failbit);
   return is;
}