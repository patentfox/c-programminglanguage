#include "Sales_data.h"
#include "cassert"
using namespace std;

std::ostream& print(std::ostream& os, const Sales_data& sd) {
   os << "(" << sd.isbn() << ", " << sd.unitsSold << ", "
      << sd.revenue << ", " << sd.avg_price() << ")";
   return os;
}

istream& read(istream& is, Sales_data& sd) {
   double price;
   is >> sd.bookNo >> sd.unitsSold >> price;
   sd.revenue = price * sd.unitsSold;
   return is;
}

double Sales_data::avg_price() const{
   if(unitsSold) {
      return revenue/unitsSold;
   }
   return 0;
}

Sales_data::Sales_data(std::istream &is) {
   read(is, *this);
}

Sales_data& Sales_data::combine(const Sales_data& item) {
   assert(item.isbn() == this->isbn());
   this->revenue += item.revenue;
   this->unitsSold += item.unitsSold;
   return *this;
}

Sales_data add(const Sales_data& item1, const Sales_data& item2) {
   Sales_data sum = item1;
   return sum.combine(item2);
}

