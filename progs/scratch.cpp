#include <vector>
#include <iostream>
#include <string>
#include <cassert>
#include <sstream>
#include <list>
#include <array>
#include <forward_list>
#include <functional>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <fstream>
#include <memory>
#include <iterator>
#include <algorithm>
#include "StrBlob.h"
#include "scratch.h"
using namespace std;
using namespace std::placeholders;

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

void test1() {
   vector<string> vs{"hello"};
   vector<string> vs2{10};
   cout << (vs > vs2) << endl;
   // vector<string> vs3 = 1; // this is an error as this constructor is explicit
   vector<int> vi{10};
   cout << vs2.size() << " " << vi.size() << endl;
}

void test2() {
   const vector<int> cv{1,2,3};
   for(auto it = cv.begin(); it != cv.end(); ++it) {
	  //*it = 10;    // error
	  cout<< *it << endl;
   }
}

// // error
// void test3() {
//    auto x[] = {1,2,3};
//    for(auto j:x)
//       cout << j << endl;
// }

void test4() {
   int arr[] = {1,2,3,4,5};
   // a2 is a simple
   auto a2(arr);
   cout << *a2 << endl;

   decltype(arr) a3;
   a3[0] = 10;
   for(int x:a3) {
	  cout << x << endl;
   }
}

// //warning
// void test5() {
//    int arr[10][20][30] = {0}; // all other elements are value initialized
//    int ia[3][4] = {
//       {1,2,3,4},
//       {5,6,7,8},
//       {9,10,11,12}
//    };
//    int (&row)[4] = ia[1];
//    int (&r)[20][30] = arr[1];
// }

// //error
// void test6() {
//    int i = 10;
//    switch(i) {
//       case 1:
//          string str;;
//          break;
//       case 10:
//          cout << "case 10" << endl;
//          break;
//       default:
//          ;
//    }
// }

void print(int i) {
   cout << i << endl;
}

void print(string j) {
   cout << j << endl;
}

void test7() {
   void print(int);
   print(10);
   // print("Kaustubh");
}

// void test8(int x=10);
// void test8(int x);
void test8(int x=10) {
   cout << x << endl;
}

void test9() {
   assert(false);
}

void test10() {
   //#ifndef NDEBUG
	  cout << "currently executing " << __func__ << " function" << endl;
   //#endif
}

void test10(int) {
   //#ifndef NDEBUG
	  cout << "currently executing " << __func__ << " function" << endl;
   //#endif
}

class classtest11 {
   int a,b;
public:
   classtest11(int x, int y) {
	  a = x;
	  b = y;
   }
   void print() {
	  cout << a << " " << b << endl;
   }
};

void test11() {
   classtest11 obj{1,2};
   obj.print();
}

void test12() {
   int v=1;
   int *p = &v;
   decltype(*p) c = v;
   cout << c << endl;
   decltype((v)) d = v;
   cout << d << endl;
}

class test13class {
public:
   test13class(int i) {

   }
};

void test13() {
   test13class obj = 12;
   (void)obj;
}

constexpr int ctest14(int a) {
   //a+=1;
   return 2*a;
}

void test14() {
   //int i = 10;
   int arr[ctest14(10)];
   for(auto x: arr) {
	  cout << x << " ";
   }
   cout << endl;
}

void test15() {
	int a = 10;
	int& b = a;
	decltype(b) c = a;
	cout << c << endl;
}

void test16() {
	vector<int> v1 = { 1,2,3,4,5 };
	vector<int> v2 = { -1,2,3,5,6 };
	cout << (v1 < v2) << endl;
}

void test17() {
	auto ai = { 1,2,3 };
	int arr[] = { 1,2,3,4 };
	decltype(arr) bi = { 1,2,3,4 };
	for (auto a : bi) {
		cout << a << endl;
	}
}

void test18() {
	// int *a = { 1,2,3,4,5,6 };
	int a[] = { 1,2,3 };
	auto b = a;
	decltype(a) c = { 1,2,3 };
	// decltype(a) d = { 1,2,3,4 };
	decltype(a) e = { 1 };
}

struct Data {
	int a;
	string s;
};

#include <type_traits>

void test19() {
	cout << std::boolalpha;
	cout << std::is_literal_type<Data>::value << endl;
}

class TestStatic {
private:
	static void function() {
		x = 10;
		cout << "static\n";
	}
public:
	static int x;
	TestStatic() {
		x = 10;
	}
};

int TestStatic::x = 20;

void test20() {
	TestStatic t;
}

struct Data21 {
	string s;
	int i;
};

class Test21 {
public:
	// static const double d = 10;
	//static constexpr Data21 d = { 10 };
};

struct Data22 {
public:
	static constexpr int i = 0;
	void call_fn();
};

void fn(int j) {
	cout << j;
}

void Data22::call_fn() {
	fn(i);
}

void test23() {
	stringstream strm("abc");
	cout << strm.str() << endl;
	strm.str("def");
	cout << strm.str() << endl;
}

void test24() {
	vector<PersonInfo> people;
	string line;
	while (getline(cin, line)) {
		istringstream ss(line);
		string name, phone;
		ss >> name;
		PersonInfo person;
		person.name = name;
		while (ss >> phone) {
			person.phones.push_back(phone);
		}
		people.push_back(person);
	}
}

void test25() {
	vector<int> v1 = { 1,2,3,4 };
	list<int> l1 = { 2,3 };
	l1.assign(v1.begin(), v1.end());

	for (auto x : l1) {
		cout << x << endl;
	}
}

template<std::size_t SIZE>
void print26(array<int, SIZE> arr) {
	for (auto x : arr) {
		cout << x << endl;
	}
}

void test26() {
	array<int, 10> ar10;
	ar10[5] = 5;
	print26(ar10);
}

void test27() {
	vector<int> v1{ 1,2,3,4 };
	vector<int> v2{ 3,4,5 };
	swap(v1, v2);
	v1.swap(v2);
	array<int, 10> ar10{ 1,2,3 };
	array<int, 5> ar5{ 3,4 };
	// error - ar10.swap(ar5);
}

void test28() {
	vector<int> v = { 1,2,3 };
	cout << v.max_size() << endl;
	array<int, 10> arr;
	cout << arr.max_size() << endl;
}

// check out resize function
void test29() {
	forward_list<int> lst(10, 42);
	lst.resize(20);
	for (auto x : lst) {
		cout << x << endl;
	}
}

// check out capacity management functions
void test30() {
	vector<int> v{ 1,2,3 };
	v.reserve(20);
	cout << v.size() << "\t" << v.capacity() << endl;
	v.reserve(2);
	cout << v.size() << "\t" << v.capacity() << endl;
	v.resize(30);
	cout << v.size() << "\t" << v.capacity() << endl;
}

// checkout substr function
void test31() {
	string s = "hello world!";
	string s2 = s.substr(10, 100);
	cout << s2 << endl;
}

// equal function
void test32() {
	vector<int> v1{ 1,2,3,4 };
	vector<int> v2{ 1,2 };
	cout << equal(v1.cbegin(), v1.cend(), v2.cbegin()) << endl;
}

void elimDups(vector<string>& words) {
	sort(words.begin(), words.end());
	auto new_end = unique(words.begin(), words.end());
	words.erase(new_end, words.end());
}

void testElimDups() {
	vector<string> v = { "the", "quick", "red", "fox", "jumps", "over", "the", "red", "turtle" };
	elimDups(v);
	for (string x : v) {
		cout << x << "\t";
	}
}

bool check_size(const string& s, vector<string>::size_type sz) {
	return s.size() >= sz;
}

// print all words with size greater than or equal to sz
void biggies(vector<string> &words, vector<string>::size_type sz) {
	elimDups(words);
	stable_sort(words.begin(), words.end(), [](const string& s1, const string& s2) {
		return s1.size() < s2.size();
	});

	// find iterator to element which has size >= sz
	auto it = find_if(words.begin(), words.end(), std::bind(check_size, placeholders::_1, sz));

	for_each(it, words.end(), [](const string& s) {
		cout << s << endl;
	});
}

void testBiggies() {
	vector<string> words = { "the", "quick", "red", "fox", "jumps", "over", "the", "red", "turtle" };
	biggies(words, 4);
}

void test33() {
	int i = 0;
	vector<int> v{ 1,2,3 };
	for_each(v.begin(), v.end(), [i](int x) mutable {
		cout << ++i << endl;
	});
}

void _test34(int i, int j) {
	cout << i << endl;
}

void test34() {
	auto callable = bind(_test34, _1, _2);
}

// using partition function
void test35() {
	string lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur nibh ipsum. Nullam rutrum egestas odio et mattis. Pellentesque mi.";
	vector<string> vs(split(lorem_ipsum, " ,.:"));

	auto it = partition(vs.begin(), vs.end(), [](const string& s) {
		return s.size() >= 4;
	});
	for_each(vs.begin(), it, [](const string& s) {
		cout << s << endl;
	});
}

bool _test36(const string& s, vector<string>::size_type sz) {
	return s.size() >= sz;
}

// using partition function
void test36() {
	string lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur nibh ipsum. Nullam rutrum egestas odio et mattis. Pellentesque mi.";
	vector<string> vs(split(lorem_ipsum, " ,.:"));
	vector<string>::size_type sz = 4;

	auto it = partition(vs.begin(), vs.end(), bind(_test36, _1, sz));
	for_each(vs.begin(), it, [](const string& s) {
		cout << s << endl;
	});
}

// test back inserter iterator
void test37() {
	string lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur nibh ipsum. Nullam rutrum egestas odio et mattis. Pellentesque mi.";
	vector<string> vs(split(lorem_ipsum, " ,.:"));
	vector<string> test;
	back_insert_iterator<vector<string>> it = back_inserter(test);
	for (auto x : vs) {
		*it = x;
	}
	for (auto x : test) {
		cout << x << endl;
	}
}

// test front inserter
void test38() {
	string lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur nibh ipsum. Nullam rutrum egestas odio et mattis. Pellentesque mi.";
	vector<string> vs(split(lorem_ipsum, " ,.:"));
	list<string> test;
	front_insert_iterator<list<string>> it = front_inserter(test);
	for (auto x : vs) {
		*it = x;
	}
	for (auto x : test) {
		cout << x << endl;
	}
}



// test inserter
void test39() {
	string lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur nibh ipsum. Nullam rutrum egestas odio et mattis. Pellentesque mi.";
	vector<string> vs(split(lorem_ipsum, " ,.:"));
	list<string> test;
	insert_iterator<list<string>> it = inserter(test, test.begin());
	for (auto x : vs) {
		*it = x;
	}
	for (auto x : test) {
		cout << x << endl;
	}
}

// test stream iterators part 1
void test40() {
	istream_iterator<int> int_it(cin);
	istream_iterator<int> int_eof;

	while (int_it != int_eof) {
		cout << *int_it << endl;
		int_it++;
	}
}

void test41() {
	string lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur nibh ipsum. Nullam rutrum egestas odio et mattis. Pellentesque mi.";
	istringstream iss(lorem_ipsum);
	istream_iterator<string> it(iss), eof;
	vector<string> vs(it, eof);

	for (auto x : vs) {
		cout << x << endl;
	}
}

// std copy
void test42() {
	string lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur nibh ipsum. Nullam rutrum egestas odio et mattis. Pellentesque mi.";
	vector<string> vs(split(lorem_ipsum, " ,.:"));
	vector<string> result;
	back_insert_iterator<vector<string>> it = back_inserter(result);
	std::copy(vs.begin(), vs.end(), it);
	for (auto x : result) {
		cout << x << endl;
	}
}

void test43() {
	string lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur nibh ipsum. Nullam rutrum egestas odio et mattis. Pellentesque mi.";
	vector<string> vs(split(lorem_ipsum, " ,.:"));
	ostream_iterator<string> osit(cout, " ");
	copy(vs.begin(), vs.end(), osit);
}

// set and multi-set
void test44() {
	vector<int> vi;
	for (int i = 1;i <= 10;++i) {
		vi.push_back(i);
		vi.push_back(i);
	}
	set<int> si(vi.begin(), vi.end());
	multiset<int> msi(vi.begin(), vi.end());
	cout << si.size() << endl << msi.size() << endl;
}

// set with custom string comparison
bool compareStringByLen(const string& a, const string& b) {
	return a.size() < b.size();
}

void test45() {
	string lorem_ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras efficitur nibh ipsum. Nullam rutrum egestas odio et mattis. Pellentesque mi.";
	vector<string> vs(split(lorem_ipsum, " ,.:"));
	multiset<string, decltype(compareStringByLen)*> sset(vs.begin(), vs.end(), &compareStringByLen);
	ostream_iterator<string> ost(cout, "\n");
	copy(sset.begin(), sset.end(), ost);
}

void test46() {
	unordered_set<string> us;
	ifstream ifs("small_words.txt");
	string word;
	while (ifs >> word) {
		us.insert(word);
	}

	cout << "Number of elements: " << us.size() << endl;
	cout << "bucket_count: " << us.bucket_count() << endl;
	cout << "max_bucket_count: " << us.max_bucket_count() << endl;

	for (size_t sz = 0;sz < us.bucket_count();++sz) {
		cout << "bucket #" << sz << ": ";
		for_each(us.begin(sz), us.end(sz), [](const string &s) {
			cout << s << "(" << hash<string>()(s) <<") ";
		});
		cout << endl;
	}
}

// starting with shared_ptr
void test47() {
	shared_ptr<string> p1 = make_shared<string>("abc");
	// make_shared works just like emplace
	shared_ptr<string> p2 = make_shared<string>(10, 'c');

	if (p2) {
		cout << *p2 << endl;
	}
}

void test48() {
	auto sp1 = make_shared<TestSharedPtr>();
	{
		auto sp2(sp1);
	}
	// cout << "About to decrese ref count to 0" << endl;
	sp1.reset(new TestSharedPtr());

	TestSharedPtr *obj = new TestSharedPtr();
	shared_ptr<TestSharedPtr> sp3 = make_shared<TestSharedPtr>(*obj);
}

void test49() {
	StrBlob sb1({"abc","def","ghi"});
	string &s = sb1.back();
	s = "pqr";
	cout << sb1.back() << endl;
}

void test50() {
	C cobj(10);
	cobj.setA(20);
	cout << cobj.getA() << endl;
}


//namespace scratch {

	int main() {
		test50();
		return 0;
	}

//}
