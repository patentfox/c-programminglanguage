#include "complex.h"
#include <iostream>
using namespace std;

complex& complex::operator*=(complex z) {
   //cout << "Multiplication logic here" << endl;
   double r = this->re*z.re - this->im*z.im;
   double i = this->re*z.im + this->im*z.re;
   this->re = r;
   this->im = i;
   return *this;
}
complex& complex::operator/=(complex z) {
   complex multiplier{ z.re, -z.im };
   double div = z.re*z.re + z.im*z.im;
   *this *= multiplier;
   this->re = this->re / div;
   this->im = this->im / div;
   return *this;
}