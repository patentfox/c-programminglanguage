#include <iostream>
#include "Vector_t.h"
using namespace std;

template<typename T>
class Less_than {
public:
   Less_than(const T& v): val(v)  {}
   bool operator()(const T& x) {    // function call operator
      return  x < val;
   }
private:
   const T val;
};

template <typename T, typename P>
int count(const T& container, P pred) {
   int counter = 0;
   for(auto x: container) {
      if(pred(x))
         ++counter;
   }
   return counter;
}

void f(const Vector<int> &v, int x) {
   cout << "Number of values less than " << x
        << " = " << count(v, [&](int y){
            return y < x;
        }) << endl;
}

int main() {
   // Less_than<int> lt42{42};
   // cout << lt42(24) << endl << lt42(43) << endl;
   f({1,2,3,4,5,6},3);
}