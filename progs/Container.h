#ifndef _container_h_
#define _container_h_

class Container {
public:
   virtual double& operator[](int) = 0;
   virtual const double& operator[](int) const = 0;
   virtual int size() const = 0;
   virtual ~Container() {}
};

#endif