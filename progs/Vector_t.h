#ifndef _vector_t_h_
#define _vector_t_h_

#include <initializer_list>
#include <stdexcept>

template<typename T>
class Vector {
public:
   using value_type = T;
   Vector(int s);
   Vector(std::initializer_list<T>);
   Vector(const Vector<T> &v);  // copy constructor
   Vector& operator=(const Vector<T> &v);   // copy assignment
   Vector(Vector<T>&& v);
   ~Vector();
   T& operator[](int i);
   const T& operator[](int i) const;
   int size() const;

private:
   int sz;
   T *elem;
};

template<typename T>
T* begin(Vector<T> &v) {
   return &v[0];
}

template<typename T>
T* end(Vector<T> &v) {
   return begin(v) + v.size();
}

template<typename T>
const T* begin(const Vector<T> &v) {
   return &v[0];
}

template<typename T>
const T* end(const Vector<T> &v) {
   return begin(v) + v.size();
}


template<typename T>
Vector<T> operator+(const Vector<T> &a, const Vector<T> &b);

#include "Vector_t.tcc"

#endif