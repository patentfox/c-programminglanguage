#include <iostream>
using namespace std;

template <typename T>
void g(T t) {
   cout << t << " ";
}

void f(){
   cout << endl;
}

template <typename head, typename... tail>
void f(head h, tail... t) {
   g(h);
   f(t...);
}

int main() {
   cout << "first: ";
   f(1, 2.2, "Hello there!");
   cout << "second: ";
   f(0.2, 'c', "yuck");
}