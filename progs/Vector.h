#ifndef _vector_h_
#define _vector_h_

#include <initializer_list>
#include <stdexcept>

class Vector {
public:
   Vector(int s);
   Vector(std::initializer_list<double>);
   Vector(const Vector &v);  // copy constructor
   Vector& operator=(const Vector &v);   // copy assignment
   Vector(Vector&& v);
   ~Vector();
   double& operator[](int i);
   const double& operator[](int i) const;
   int size() const;
private:
   int sz;
   double *elem;
};

Vector operator+(const Vector &a, const Vector &b);

#endif