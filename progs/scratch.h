#include <string>
#include <vector>
#include <cstring>
#include <iostream>
using namespace std;

struct PersonInfo {
	string name;
	vector<string> phones;
};

vector<string> split(string str, string delims) {
	char* cstr = new char[str.size() + 1];
	strcpy(cstr, str.c_str());
	vector<string> vs;
	char *p = strtok(cstr, delims.c_str());
	while (p) {
		vs.push_back(p);
		p = strtok(nullptr, delims.c_str());
	}
	return vs;
}

class TestSharedPtr {
	int i;
public:
	TestSharedPtr() {
		std::cout << "Class object created" << endl;	
	}
	~TestSharedPtr() {
		std::cout << "Class object destroyed" << endl;
	}
};


class A {
private:
	int a;
public:
	void set(int x) {
		a = x;
	}	
	int get() const {
		return a;
	}
};

// class B {
// private:
// 	A mA;
// public:
// 	B(int x) {
// 		mA.set(x);
// 	}
// 	void setA(int x) const {
// 		mA.set(x);
// 	}
// 	int getA() const {
// 		return mA.get();
// 	}
// };

class C {
private:
	A *mA;
public:
	C(int x) {
		mA = new A();
		mA->set(x);
	}
	~C() {
		delete mA;
	}
	void setA(int x) const {
		mA->set(x);
	}
	int getA() const {
		return mA->get();
	}
};

